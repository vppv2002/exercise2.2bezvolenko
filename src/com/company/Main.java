package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Bank privatBank = new Bank();
        privatBank.USD = 28.5f;
        privatBank.EUR = 31.55f;
        privatBank.RUB = 0.365f;

        Bank ochadBank = new Bank();
        ochadBank.USD = 28.65f;
        ochadBank.EUR = 31.2f;
        ochadBank.RUB = 0.38f;

        Bank pumb = new Bank();
        pumb.USD = 28.5f;
        pumb.EUR = 31.5f;
        pumb.RUB = 0.355f;

        String Privat = "ПриватБанк";
        String Oshad = "ОщадБанк";
        String PUMB = "ПУМБ";
        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        Scanner scan = new Scanner(System.in);

        System.out.println("Введите количесвто денег, которое хотите обменять: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Введите банк через который хотите совершить обмен (ПриватБанк, ОщадБанк или ПУМБ): ");
        String bank = scan.nextLine();

        System.out.println("Введите валюту (USD, EUR или RUB): ");
        String currency = scan.nextLine();

        if (Privat.equalsIgnoreCase(bank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / privatBank.USD));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / privatBank.EUR));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / privatBank.RUB));
            } else {
                System.err.println("Невозможно сделать обмен " + currency);
            }
        } else if (Oshad.equalsIgnoreCase(bank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / ochadBank.USD));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / ochadBank.EUR));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / ochadBank.RUB));
            } else {
                System.err.println("Невозможно сделать обмен " + currency);
            }
        } else if (PUMB.equalsIgnoreCase(bank)) {
            if (USD.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / pumb.USD));
            } else if (EUR.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / pumb.EUR));
            } else if (RUB.equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / pumb.RUB));
            } else {
                System.err.println("Невозможно сделать обмен " + currency);
            }
        } else {
            System.err.println("Невозможно сделать обмен " + bank);
        }
    }
}